import cv2
import os
import numpy as np
import itertools
from skimage import measure
from skimage.morphology import skeletonize
import math


param_thresh_values = [0.200, 0.175, 0.150, 0.125, 0.1]
param_ksize_values = [5, 7, 9, 11, 13]


def filter(img_binary):
    kernel = np.ones((3, 3), np.uint8)
    img_binary = cv2.morphologyEx(img_binary, cv2.MORPH_CLOSE, kernel)

    labels = measure.label(img_binary, neighbors=8, background=0)
    mask = np.zeros(img_binary.shape, dtype="uint8")

    # loop over the unique components
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue

        # otherwise, construct the label mask and count the
        # number of pixels
        labelMask = np.zeros(img_binary.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)

        # if the number of pixels in the component is sufficiently
        # large, then add it to our mask of "large blobs"
        if numPixels > 2:
            mask = cv2.add(mask, labelMask)
    return mask


def thresh_img(img_gray):
    img_gray = cv2.medianBlur(img_gray, 7)
    img_thresh = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 69, 70)
    return img_thresh


def get_skeleton(img_binary):
    img_bool = img_binary > 128
    output = skeletonize(img_bool)
    output = np.array(output, dtype=np.uint8) * 255
    return output


# Harris corner detector
def corners_detection1(img_binary):
    img_float = np.float32(img_binary)
    dst = cv2.cornerHarris(img_float, 2, 13, 0)
    print(dst.shape, dst.dtype, np.min(dst), np.mean(dst), np.max(dst))
    dst = np.array(255 * (np.min(dst) + dst) / (np.max(dst) - np.min(dst)), np.uint8)
    print(dst.shape, dst.dtype, np.min(dst), np.mean(dst), np.max(dst))
    # _, dst = cv2.threshold(dst, 100, 255, cv2.THRESH_BINARY)
    print(dst.shape, dst.dtype, np.min(dst), np.mean(dst), np.max(dst))
    print(dst)
    # dst = cv2.dilate(dst, None)
    cv2.imwrite("input.jpg", img_binary)
    cv2.imwrite("output.jpg", dst)
    input("hi")
    return None


# Shi-Tomasi corner detector
def corners_detection2(img_binary, param_thresh, param_ksize):
    corners = cv2.goodFeaturesToTrack(image=img_binary,
                                      maxCorners=10000,
                                      qualityLevel=param_thresh,
                                      minDistance=10,
                                      blockSize=param_ksize)

    corners_list = []
    for corner in corners:
        x, y = corner.ravel()
        corners_list.append((int(y), int(x)))

    """  visualization
    img_result = cv2.cvtColor(img_binary, cv2.COLOR_GRAY2BGR)
    for corner in corners:
        x, y = corner.ravel()
        cv2.circle(img_result, (x, y), 3, 255, -1)
    cv2.imwrite("input.jpg", img_binary)
    cv2.imwrite("output.jpg", img_result)
    input("hi")
    """
    return corners_list


def find_points(img_binary, param_thresh, param_ksize):
    img_binary = get_skeleton(img_binary)
    corners = corners_detection2(img_binary, param_thresh, param_ksize)
    return corners


def read_points(file_path):
    points = []
    with open(file_path) as f:
        for line in f.readlines():
            line = line[:-1]
            if line != '':
                y, x = line.split(' ')
                points.append((int(y), int(x)))
    return points


def normalize_points(points, box_x, box_y, box_w, box_h):
    for i in range(len(points)):
        points[i] = ((points[i][0] - box_y) / box_h, (points[i][1] - box_x) / box_w)
    return points


def get_dist(point1, point2):
    return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)


def get_min_dist(point, points_set):
    min_dist = float("inf")
    for i in range(len(points_set)):
        dist = get_dist(point, points_set[i])
        if dist < min_dist:
            min_dist = dist
    return min_dist


def get_loss(output_points, target_points):
    # If too many output points, don't calc pairwise distance
    # if len(target_points) * 3 < len(output_points):
    #     print("Too many outputs", len(target_points), len(output_points))
    #     return float("inf")

    dist_o2t = 0
    for point in output_points:
        dist_o2t += get_min_dist(point, target_points) / len(output_points)

    dist_t2o = 0
    for point in target_points:
        dist_t2o += get_min_dist(point, output_points) / len(target_points)

    return dist_o2t + dist_t2o


min_loss = float("inf")
min_loss_params = None
for param_thresh, param_ksize in itertools.product(param_thresh_values, param_ksize_values):
    total_loss = 0
    for image_name in os.listdir("inputs"):
        img_bgr = cv2.imread(os.path.join("inputs", image_name))
        img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
        groundtruth_points = read_points(os.path.join("points", image_name[:-3] + "txt"))

        img_preproc = thresh_img(img_gray)  # black background, white walls
        img_preproc = filter(img_preproc)
        img_preproc = 255 - img_preproc

        output_points = find_points(img_preproc, param_thresh, param_ksize)

        h, w, c = img_bgr.shape
        loss = get_loss(normalize_points(output_points, 0, 0, w, h), (normalize_points(groundtruth_points, 0, 0, w, h)))

        total_loss += loss / len(os.listdir("inputs"))
    print("Loss {}".format(total_loss))
    print("Params", param_thresh, param_ksize)
    if total_loss < min_loss:
        min_loss = total_loss
        min_loss_params = (param_thresh, param_ksize)

print("Best params", min_loss_params)
print("With loss", min_loss)
