import cv2
import os
import numpy as np
import math
import tensorflow as tf
from skimage import measure
from skimage.morphology import skeletonize

os.environ["CUDA_VISIBLE_DEVICES"] = '0'

crop_rate = 1 / 10
crop_final_size = 64
stride = 64

predict_fn = tf.contrib.predictor.from_saved_model("nn_segm_half")


def process_segm_crop(crop):
    img_rgb = cv2.cvtColor(crop, cv2.COLOR_BGR2RGB)
    output_img = predict_fn({"img": img_rgb})["output_segm_img"][0, :, :, :]
    return output_img


def process_segm(img_name):
    input_path = os.path.join("inputs", img_name)
    output_path = os.path.join("nn_outputs", img_name)

    print("processing", input_path)

    img = cv2.imread(input_path)
    h, w, c = img.shape
    walls = np.zeros((h, w), dtype=np.float32)
    crop_size = int(crop_rate * max(h, w))
    for h_i in range((h - crop_size) // stride + 1):
        y = h_i * stride
        for w_i in range((w - crop_size) // stride + 1):
            x = w_i * stride
            crop = img[y: y + crop_size, x: x + crop_size]
            crop = cv2.resize(crop, (crop_final_size, crop_final_size))
            walls_crop = process_segm_crop(crop)
            walls_crop = cv2.resize(walls_crop, (crop_size, crop_size), interpolation=cv2.INTER_NEAREST)
            walls[y: y + crop_size, x: x + crop_size] += walls_crop
    walls /= math.ceil(crop_size / stride) ** 2
    walls = np.array(walls, dtype=np.uint8)

    ret, walls = cv2.threshold(walls, 50, 255, cv2.THRESH_BINARY)

    cv2.imwrite(output_path, walls)
    return walls


def filter(img_binary):
    kernel = np.ones((3, 3), np.uint8)
    img_binary = cv2.morphologyEx(img_binary, cv2.MORPH_CLOSE, kernel)

    labels = measure.label(img_binary, neighbors=8, background=0)
    mask = np.zeros(img_binary.shape, dtype="uint8")

    # loop over the unique components
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue

        # otherwise, construct the label mask and count the
        # number of pixels
        labelMask = np.zeros(img_binary.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)

        # if the number of pixels in the component is sufficiently
        # large, then add it to our mask of "large blobs"
        if numPixels > 2:
            mask = cv2.add(mask, labelMask)
    return mask


def get_skeleton(img_binary):
    img_bool = img_binary > 128
    output = skeletonize(img_bool)
    output = np.array(output, dtype=np.uint8) * 255
    return output


# Shi-Tomasi corner detector
def corners_detection2(img_binary):
    corners = cv2.goodFeaturesToTrack(image=img_binary,
                                      maxCorners=10000,
                                      qualityLevel=0.09,
                                      minDistance=10,
                                      blockSize=5)

    corners_list = []
    for corner in corners:
        x, y = corner.ravel()
        corners_list.append((int(y), int(x)))
    return corners_list


def find_points(img_binary):
    img_binary = get_skeleton(img_binary)
    corners = corners_detection2(img_binary)
    return corners


def read_points(file_path):
    points = []
    with open(file_path) as f:
        for line in f.readlines():
            line = line[:-1]
            if line != '':
                y, x = line.split(' ')
                points.append((int(y), int(x)))
    return points


def normalize_points(points, box_x, box_y, box_w, box_h):
    for i in range(len(points)):
        points[i] = ((points[i][0] - box_y) / box_h, (points[i][1] - box_x) / box_w)
    return points


def get_dist(point1, point2):
    return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)


def get_min_dist(point, points_set):
    min_dist = float("inf")
    for i in range(len(points_set)):
        dist = get_dist(point, points_set[i])
        if dist < min_dist:
            min_dist = dist
    return min_dist


def get_loss(output_points, target_points):
    # If too many output points, don't calc pairwise distance
    # if len(target_points) * 3 < len(output_points):
    #     print("Too many outputs", len(target_points), len(output_points))
    #     return float("inf")

    dist_o2t = 0
    for point in output_points:
        dist_o2t += get_min_dist(point, target_points) / len(output_points)

    dist_t2o = 0
    for point in target_points:
        dist_t2o += get_min_dist(point, output_points) / len(target_points)

    return dist_o2t + dist_t2o


def get_iou(output_img, target_img):
    bool_output_img = output_img > 128
    bool_target_img = target_img > 128

    pos_output = np.count_nonzero(bool_output_img)
    true_pos = np.count_nonzero(np.logical_and(bool_output_img, bool_target_img))
    false_pos = pos_output - true_pos

    neg_output = np.count_nonzero(np.logical_not(bool_output_img))
    true_neg = np.count_nonzero(np.logical_not(np.logical_or(bool_target_img, bool_output_img)))
    false_neg = neg_output - true_neg

    iou = (true_pos + 0.00001) / (true_pos + false_pos + false_neg + 0.00001)

    return iou


total_acc = 0
total_loss = 0
for image_name in os.listdir("inputs"):
    if not os.path.exists('nn_outputs'):
        os.makedirs('nn_outputs')
    if not os.path.exists('nn_outputs'):
        os.makedirs('nn_outputs')

    img_bgr = cv2.imread(os.path.join("inputs", image_name))
    groundtruth = cv2.imread(os.path.join("walls", image_name), cv2.IMREAD_GRAYSCALE)
    groundtruth_points = read_points(os.path.join("points", image_name[:-3] + "txt"))

    nn_output = process_segm(image_name)
    output_points = find_points(nn_output)

    for corner in output_points:
        y, x = corner
        cv2.circle(img_bgr, (x, y), 3, 255, -1)
    cv2.imwrite(os.path.join("outputs2", image_name), img_bgr)

    total_acc += get_iou(nn_output, groundtruth) / len(os.listdir("inputs"))

    h, w, c = img_bgr.shape
    loss = get_loss(normalize_points(output_points, 0, 0, w, h), (normalize_points(groundtruth_points, 0, 0, w, h)))

    total_loss += loss / len(os.listdir("inputs"))

print("segm acc {}".format(total_acc))
print("loss {}".format(total_loss))


