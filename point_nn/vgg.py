import tensorflow as tf
from point_nn.architectures import lrelu, conv_block


def vgg_encoder_block(input_tensor: tf.Tensor, filters, n_convs, af, train_mode):
    conv = input_tensor
    for i in range(n_convs):
        conv = conv_block(conv, 3, train_mode=train_mode, filters=filters, af=af, bn=True,
                          kernel_regularizer=tf.contrib.layers.l2_regularizer(1.0))
    pool3 = tf.layers.max_pooling2d(conv, 2, 2, 'same')
    return pool3


def nn_vgg16(input_tensor: tf.Tensor, output_channels, training, dropout_rate=0.0):
    """
    Note: it is different from the original one in utilization of lrelu instead of relu.
    Also fc layers have another number of units. Ngf differs. And here we use 3 OUTPUT FC layers.
    :param input_tensor:
    :param output_channels:
    :param training:
    :return:
    """
    ngf = 16
    af = lrelu
    print("input_tensor", input_tensor.get_shape())
    enc1 = vgg_encoder_block(input_tensor, ngf, 2, af, training)  # os=2
    enc2 = vgg_encoder_block(enc1, ngf * 2, 2, af, training)  # os=4
    enc3 = vgg_encoder_block(enc2, ngf * 4, 3, af, training)  # os=8
    enc4 = vgg_encoder_block(enc3, ngf * 8, 3, af, training)  # os=16
    enc5 = vgg_encoder_block(enc4, ngf * 8, 3, af, training)  # os=32

    h = int(enc5.get_shape()[1])
    w = int(enc5.get_shape()[2])
    c = int(enc5.get_shape()[3])
    enc5 = tf.reshape(enc5, [-1, h*w*c])

    dense1 = tf.layers.dense(enc5, 512, kernel_regularizer=tf.contrib.layers.l2_regularizer(1.0))
    dense1 = lrelu(dense1)
    dense1 = tf.layers.dropout(dense1, rate=dropout_rate, training=training)

    dense2 = tf.layers.dense(dense1, 512, kernel_regularizer=tf.contrib.layers.l2_regularizer(1.0))
    dense2 = lrelu(dense2)
    dense2 = tf.layers.dropout(dense2, rate=dropout_rate, training=training)

    result_0 = tf.layers.dense(dense2, output_channels, kernel_regularizer=tf.contrib.layers.l2_regularizer(1.0))
    result_1 = tf.layers.dense(dense2, output_channels, kernel_regularizer=tf.contrib.layers.l2_regularizer(1.0))
    result_2 = tf.layers.dense(dense2, output_channels, kernel_regularizer=tf.contrib.layers.l2_regularizer(1.0))
    return result_0, result_1, result_2
