import cv2
import os
import numpy as np
import math
import tensorflow as tf

crop_rate = 1 / 10
crop_final_size = 64
stride = 8
p_thresh = 0.5
input_channels = 1


def process_crop(crop, model, p_thresh=0.5):
    if input_channels == 3:
        crop_rgb = cv2.cvtColor(crop, cv2.COLOR_BGR2RGB)
    else:
        crop_rgb = crop
        crop_rgb = np.expand_dims(crop_rgb, 2)
    prediction = model({'img': crop_rgb})
    prediction_y = prediction['output_predictions_y'][0]
    prediction_x = prediction['output_predictions_x'][0]
    prediction_p = prediction['output_predictions_p'][0]
    if prediction_p[0] > p_thresh:
        return prediction_y[0], prediction_x[0]
    else:
        return None


def process(img_name, input_folder, save_folder, model):
    input_path = os.path.join(input_folder, img_name)
    output_path = os.path.join(save_folder, img_name)

    print("processing", input_path)

    img = cv2.imread(input_path)
    if input_channels == 1:
        input_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        h, w = input_img.shape
    else:
        input_img = img
        h, w, c = img.shape
    crop_size = int(crop_rate * max(h, w))
    for h_i in range((h - crop_size) // stride + 1):
        y = h_i * stride
        for w_i in range((w - crop_size) // stride + 1):
            x = w_i * stride
            crop = input_img[y: y + crop_size, x: x + crop_size]
            crop = cv2.resize(crop, (crop_final_size, crop_final_size))
            point = process_crop(crop, model, p_thresh=p_thresh)
            if point is not None:
                real_x = int(x + point[1] * crop_size)
                real_y = int(y + point[0] * crop_size)
                cv2.circle(img, (real_x, real_y), 3, (255, 0, 0), -1)

    cv2.imwrite(output_path, img)


def test(model, test_data_folder, save_folder):
    predict_fn = tf.contrib.predictor.from_saved_model(model)
    files = os.listdir(test_data_folder)
    if not os.path.exists(save_folder):
        os.mkdir(save_folder)
    for file in files:
        process(file, test_data_folder, save_folder, predict_fn)
