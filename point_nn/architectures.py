"""
file for general neural blocks and nets
"""
import tensorflow as tf

default_w_initializer = tf.glorot_uniform_initializer()


def lrelu(x, alpha=0.2):
    return tf.maximum(alpha*x, x)


def conv_block(input_tensor: tf.Tensor, kernel_size: int, train_mode, stride=1, filters=None, name=None, rate=1,
               use_bias=True, af=None, bn=False, order_bn_relu_conv=False, kernel_regularizer=None):
    """

    :param kernel_regularizer:
    :param input_tensor:
    :param kernel_size:
    :param stride:
    :param filters:
    :param name:
    :param rate:
    :param use_bias:
    :param af:
    :param bn:
    :param order_bn_relu_conv: if False, traditional order conv-bn-relu will be applied
    :return:
    """
    input_tensor1 = input_tensor
    if order_bn_relu_conv:
        if bn:
            input_tensor1 = tf.layers.batch_normalization(input_tensor1, training=train_mode)
        if af is not None:
            input_tensor1 = af(input_tensor1)

    conv1 = tf.layers.conv2d(
        inputs=input_tensor1,
        filters=filters,
        kernel_size=[kernel_size, kernel_size],
        kernel_initializer=default_w_initializer,
        strides=[stride, stride],
        padding="same",
        dilation_rate=(rate, rate),
        use_bias=use_bias,
        kernel_regularizer=kernel_regularizer,
        name=name
    )
    if not order_bn_relu_conv:
        if bn:
            conv1 = tf.layers.batch_normalization(conv1, training=train_mode)
        if af is not None:
            conv1 = af(conv1)
    '''
    metadata = {
        "name": name,
        "kernel_size": kernel_size,
        "input_depth": input_tensor.get_shape()[3],
        "filters": filters,
        "stride": stride,
        "input_shape": input_tensor.get_shape()
    }
    return conv1, metadata
    '''
    return conv1