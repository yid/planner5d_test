import tensorflow as tf
import os
from point_nn.vgg import nn_vgg16 as nn
import random
from point_nn import test

"""
This version is regression of 1 point for a cropped version. Also note that here we decided to operate like this: 
if we have None as annotation, we sent it to nn as <random y, random x, p=0>, but optimize only for loss function 
for p.
"""

MODEL_PATH = "model2_after_segm"
EPOCHS = 10
TEST_N_EPOCHS = 1
TRAIN_N_TIMES = int(EPOCHS / TEST_N_EPOCHS)
TRAIN_DATASET_PATH = "test_assignment_21-00/MyNN_segm/DATA_full/train_after_segm"
TRAIN_TFRECORDS = "test_assignment_21-00/MyNN_segm/DATA_full/train_after_segm/train.tfrecords"
TEST_TFRECORDS = "test_assignment_21-00/MyNN_segm/DATA_full/test_after_segm/test.tfrecords"
VAL_DATASET_PATH = "test_assignment_21-00/MyNN_segm/DATA_full/test_after_segm"
IMAGES_FOLDER = "images"
LABELS_FOLDER = "labels"
TEST_DATASET_PATH = "test_assignment_21-00/segm_nn_outputs"
BATCH_SIZE = 32
NUM_POINTS = 1
L2_REG_SCALE = 0.0
LR = 0.00002
FINE_SIZE = (64, 64)
INPUT_CHANNELS = 1
run_config = tf.contrib.learn.RunConfig(save_summary_steps=10,
                                        save_checkpoints_secs=100)


def deprocess_data(image_tensor):
    image_tensor = tf.cast(image_tensor, tf.uint8)
    return image_tensor


def model(features, labels, mode):
    input_img = features["input_img"]
    if mode != tf.estimator.ModeKeys.PREDICT:
        labels_y = features['labels_y']
        labels_x = features['labels_x']
        labels_p = features['labels_p']
    else:
        labels_y, labels_x, labels_p = None, None, None

    input_image_norm = input_img
    input_image_norm = input_image_norm - tf.reduce_mean(input_image_norm, axis=[1, 2, 3], keep_dims=True)

    output_y, output_x, output_p = nn(input_image_norm, NUM_POINTS, (mode == tf.estimator.ModeKeys.TRAIN))
    output_y = tf.nn.sigmoid(output_y)
    output_x = tf.nn.sigmoid(output_x)
    output_p = tf.nn.sigmoid(output_p)
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {"output_predictions_y": tf.identity(output_y, name="output_predictions_y"),
                       "output_predictions_x": tf.identity(output_x, name="output_predictions_x"),
                       "output_predictions_p": tf.identity(output_p, name="output_predictions_p")}
        export_outputs = {"export_output": tf.estimator.export.PredictOutput(predictions)}
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs=export_outputs)

    total_loss_p = tf.losses.mean_squared_error(labels_p, output_p)
    total_loss_p += L2_REG_SCALE * tf.losses.get_regularization_loss()

    total_loss_y = tf.reduce_mean(tf.pow(labels_y - output_y, 2) * labels_p)
    total_loss_y += L2_REG_SCALE * tf.losses.get_regularization_loss()

    total_loss_x = tf.reduce_mean(tf.pow(labels_x - output_x, 2) * labels_p)
    total_loss_x += L2_REG_SCALE * tf.losses.get_regularization_loss()
    total_loss = total_loss_p + total_loss_y + total_loss_x


    # todo think about train_mae
    with tf.variable_scope("losses"):
        tf.summary.scalar("total_loss", total_loss)

    with tf.variable_scope("images_control"):
        tf.summary.image("input_image", deprocess_data(input_img), max_outputs=3)

    if mode == tf.estimator.ModeKeys.EVAL:
        eval_mae_y = tf.metrics.mean_absolute_error(labels_y, output_y)
        eval_mae_x = tf.metrics.mean_absolute_error(labels_x, output_x)
        eval_mae_p = tf.metrics.mean_absolute_error(labels_p, output_p)
        eval_metric_ops = {"losses/eval_mae_y": eval_mae_y,
                           "losses/eval_mae_x": eval_mae_x,
                           "losses/eval_mae_p": eval_mae_p}
        return tf.estimator.EstimatorSpec(mode=mode,
                                          loss=total_loss,
                                          eval_metric_ops=eval_metric_ops)

    else:
        optimizer = tf.train.AdamOptimizer(learning_rate=LR, beta1=0.9, beta2=0.999)
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            train_op = optimizer.minimize(total_loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=total_loss, train_op=train_op)


def __decode_resize_encode(path, channels, size=FINE_SIZE, img_format="jpg"):
    assert img_format in ["jpg", "png"], "Not supported format {}".format(img_format)
    file = tf.read_file(path)
    img = tf.image.decode_jpeg(file, channels=channels)

    # inputs padding instead of unproportional resizing (only square sizes supported)
    max_dim = tf.where(tf.shape(img)[0] > tf.shape(img)[1], tf.shape(img)[0], tf.shape(img)[1])
    img = tf.image.resize_image_with_crop_or_pad(img, max_dim, max_dim)

    img = tf.image.resize_images(img, size)
    img = tf.cast(img, tf.uint8)
    if img_format == "jpg":
        encoded_img = tf.image.encode_jpeg(img, "rgb" if channels == 3 else "grayscale",
                                           optimize_size=True)
    else:
        encoded_img = tf.image.encode_png(img)
    return encoded_img


def _create_tfrecord(tfrecord_path, dataset_folder):
    train_images_files = os.listdir(os.path.join(dataset_folder, IMAGES_FOLDER))
    data_pairs = []
    for file_name in train_images_files:
        file_name_full = os.path.join(dataset_folder, IMAGES_FOLDER, file_name)
        labels_path = os.path.join(dataset_folder, LABELS_FOLDER, os.path.splitext(file_name)[0] + ".txt")
        with open(labels_path, "r") as f:
            y, x, p = 0, 0, 0
            for line in f:
                line1 = line.rstrip()
                if line1 != '':
                    if line1 != 'None':
                        temp_lst = list(map(float, line1.split(' ')))
                        y = temp_lst[0]
                        x = temp_lst[1]
                        p = 1.0
                    else:
                        y = random.uniform(0, 1)
                        x = random.uniform(0, 1)
                        p = 0.0
                break
        data_pairs.append((file_name_full, y, x, p))

    print('Writing', tfrecord_path)
    with tf.Graph().as_default(), tf.Session() as sess, tf.python_io.TFRecordWriter(tfrecord_path) as writer:
        input_img_path_ph = tf.placeholder(tf.string)
        input_img_jpg_t = __decode_resize_encode(input_img_path_ph, INPUT_CHANNELS, FINE_SIZE, "jpg")

        for pair in data_pairs:
            f = {}
            try:
                input_img_jpg = sess.run(input_img_jpg_t, {input_img_path_ph: pair[0]})
            except tf.errors.InvalidArgumentError:
                print("Corrupted file", pair[0])
                continue
            f['input_img_jpg'] = tf.train.Feature(bytes_list=tf.train.BytesList(value=[input_img_jpg]))

            f['y'] = tf.train.Feature(float_list=tf.train.FloatList(value=[pair[1]]))
            f['x'] = tf.train.Feature(float_list=tf.train.FloatList(value=[pair[2]]))
            f['p'] = tf.train.Feature(float_list=tf.train.FloatList(value=[pair[3]]))

            example = tf.train.Example(features=tf.train.Features(feature=f))
            writer.write(example.SerializeToString())


def _input_fn(epochs, dataset_folder, tfrecord_path):
    def parse(record):
        feature = {
            'input_img_jpg': tf.FixedLenFeature([], tf.string),
            'y': tf.FixedLenFeature([], tf.float32),
            'x': tf.FixedLenFeature([], tf.float32),
            'p': tf.FixedLenFeature([], tf.float32)
        }
        tf_record_features = tf.parse_single_example(record, features=feature)

        def decode(img, channels):
            img = tf.image.decode_jpeg(img, channels=channels)
            img = tf.reshape(img, [FINE_SIZE[0], FINE_SIZE[1], channels])
            img = tf.cast(img, tf.float32)  # unneccecary, because this cast in augmentation
            return img

        input_img = decode(tf_record_features['input_img_jpg'], channels=INPUT_CHANNELS)
        y = tf.cast(tf_record_features['y'], tf.float32)
        y = tf.expand_dims(y, 0)
        x = tf.cast(tf_record_features['x'], tf.float32)
        x = tf.expand_dims(x, 0)
        p = tf.cast(tf_record_features['p'], tf.float32)
        p = tf.expand_dims(p, 0)
        return input_img, y, x, p

    dataset = tf.data.TFRecordDataset(tfrecord_path)
    dataset = dataset.map(map_func=parse, num_parallel_calls=min(BATCH_SIZE, 8))
    dataset = dataset.batch(BATCH_SIZE)
    dataset = dataset.prefetch(4)
    dataset = dataset.repeat(epochs)
    iterator = dataset.make_one_shot_iterator()
    img_pairs = iterator.get_next()

    features = {'input_img': img_pairs[0], 'labels_y': img_pairs[1], 'labels_x': img_pairs[2], 'labels_p': img_pairs[3]}
    labels = None
    return features, labels


def serving_img_input_fn():
    img = tf.placeholder(dtype=tf.float32, shape=[FINE_SIZE[0], FINE_SIZE[1], INPUT_CHANNELS], name='input_img_tensor')
    receiver_tensors = {'img': img}
    img = tf.expand_dims(img, 0)
    features = {'input_img': img}
    return tf.estimator.export.ServingInputReceiver(features, receiver_tensors)


def train_input_fn():
    if not (os.path.isfile(TRAIN_TFRECORDS) and os.path.isfile(TEST_TFRECORDS)):
        _create_tfrecord(TRAIN_TFRECORDS, TRAIN_DATASET_PATH)
        _create_tfrecord(TEST_TFRECORDS, VAL_DATASET_PATH)
    return _input_fn(TEST_N_EPOCHS, TRAIN_DATASET_PATH, TRAIN_TFRECORDS)


def test_input_fn():
    return _input_fn(1, VAL_DATASET_PATH, TEST_TFRECORDS)


def main(unused):
    if not os.path.exists(MODEL_PATH):
        os.mkdir(MODEL_PATH)
    est = tf.estimator.Estimator(model_fn=model, model_dir=MODEL_PATH, config=run_config)
    for i in range(TRAIN_N_TIMES):
        est.train(input_fn=train_input_fn)
        est.evaluate(input_fn=test_input_fn)
        exp_model = est.export_savedmodel(os.path.join(MODEL_PATH, "exported_model_img"), serving_img_input_fn)
        exp_model = exp_model.decode()
        test.test(exp_model, TEST_DATASET_PATH, os.path.join(exp_model, "plots"))


if __name__ == "__main__":
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"
    tf.app.run()

