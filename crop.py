import cv2
import os
import random

num_crops_per_image = 10000
crop_rate_bounds = (1 / 12, 1 / 6)
crop_final_size = 64


def augment_rotate(img, walls, center_point):
    angle = random.choice([0, 90, 180, 270])
    if angle == 0:
        return img, walls, center_point
    else:
        mat = cv2.getRotationMatrix2D((crop_final_size / 2, crop_final_size / 2), angle, 1.0)
        img = cv2.warpAffine(img, mat, (crop_final_size, crop_final_size))
        walls = cv2.warpAffine(walls, mat, (crop_final_size, crop_final_size))
        if center_point is not None:
            if angle == 90:
                center_point = (1 - center_point[1], center_point[0])
            elif angle == 270:
                center_point = (center_point[1], 1 - center_point[0])
            else:  # 180
                center_point = (1 - center_point[0], 1 - center_point[1])
        return img, walls, center_point


def augment_flip(img, walls, center_point):
    choice = random.choice([-1, 0, 1, 2])
    if choice != 0:
        img = cv2.flip(img, 0)
        walls = cv2.flip(walls, 0)
        if center_point is not None:
            center_point = (1 - center_point[0], center_point[1])
    elif choice == 1:
        img = cv2.flip(img, 1)
        walls = cv2.flip(walls, 1)
        if center_point is not None:
            center_point = (center_point[0], 1 - center_point[1])
    elif choice == -1:
        img = cv2.flip(img, -1)
        walls = cv2.flip(walls, -1)
        if center_point is not None:
            center_point = (1 - center_point[0], 1 - center_point[1])
    return img, walls, center_point


def augment(img, walls, center_point):
    img, walls, center_point = augment_rotate(img, walls, center_point)
    img, walls, center_point = augment_flip(img, walls, center_point)
    return img, walls, center_point


def get_points_in_box(points, box_x, box_y, box_w, box_h):
    points_in_box = []
    for point in points:
        if box_y <= point[0] < box_y + box_h and box_x <= point[1] < box_x + box_w:
            points_in_box.append(point)
    return points_in_box


def normalize_points(points, box_x, box_y, box_w, box_h):
    for i in range(len(points)):
        points[i] = ((points[i][0] - box_y) / box_h, (points[i][1] - box_x) / box_w)
    return points


def get_center_point(points):
    min_dist = float("inf")
    min_dist_idx = -1
    for i in range(len(points)):
        dist = (points[i][0] - 0.5)**2 + (points[i][1] - 0.5)**2
        if dist < min_dist:
            min_dist = dist
            min_dist_idx = i

    if min_dist_idx < 0:
        return None
    else:
        return points[min_dist_idx]


def read_points(file_path):
    points = []
    with open(file_path) as f:
        for line in f.readlines():
            line = line[:-1]
            if line != '':
                y, x = line.split(' ')
                points.append((int(y), int(x)))
    return points


def write_point(point, file_path):
    with open(file_path, 'w') as f:
        if point is None:
            f.write("None")
        else:
            f.write("{} {}".format(point[0], point[1]))


def crop(img_name, half=False):
    img_path = os.path.join("inputs", img_name)
    walls_path = os.path.join("walls", img_name)
    points_path = os.path.join("points", img_name[:-3] + "txt")

    print("processing", img_path)

    img = cv2.imread(img_path)
    walls = cv2.imread(walls_path)
    points = read_points(points_path)

    h, w, c = img.shape
    if half:
        if h > w:
            h = h // 2
        else:
            w = w // 2
    bigger_size = max(h, w)

    for i in range(num_crops_per_image):
        crop_img_path = os.path.join("inputs_cropped_half" if half else "inputs_cropped_full",
                                     "{}_{}".format(i, img_name))
        crop_walls_path = os.path.join("walls_cropped_half" if half else "walls_cropped_full",
                                       "{}_{}".format(i, img_name))
        crop_points_path = os.path.join("points_cropped_half" if half else "points_cropped_full",
                                        "{}_{}".format(i, img_name[:-3] + "txt"))

        crop_size = int(bigger_size *
                        (crop_rate_bounds[0] + random.random() * (crop_rate_bounds[1] - crop_rate_bounds[0])))
        crop_x = random.randint(0, w - crop_size)
        crop_y = random.randint(0, h - crop_size)
        crop_img = img[crop_y: crop_y + crop_size, crop_x: crop_x + crop_size]
        crop_img = cv2.resize(crop_img, (crop_final_size, crop_final_size))
        crop_walls = walls[crop_y: crop_y + crop_size, crop_x: crop_x + crop_size]
        crop_walls = cv2.resize(crop_walls, (crop_final_size, crop_final_size))
        crop_points = get_points_in_box(points, crop_x, crop_y, crop_size, crop_size)
        crop_points_norm = normalize_points(crop_points, crop_x, crop_y, crop_size, crop_size)
        crop_center_point_norm = get_center_point(crop_points_norm)

        crop_img, crop_walls, crop_center_point_norm = augment(crop_img, crop_walls, crop_center_point_norm)

        cv2.imwrite(crop_img_path, crop_img)
        cv2.imwrite(crop_walls_path, crop_walls)
        write_point(crop_center_point_norm, crop_points_path)


for file_name in os.listdir("inputs"):
    if not os.path.exists('inputs_cropped_full'):
        os.makedirs('inputs_cropped_full')
    if not os.path.exists('walls_cropped_full'):
        os.makedirs('walls_cropped_full')
    if not os.path.exists('points_cropped_full'):
        os.makedirs('points_cropped_full')
    if not os.path.exists('inputs_cropped_half'):
        os.makedirs('inputs_cropped_half')
    if not os.path.exists('walls_cropped_half'):
        os.makedirs('walls_cropped_half')
    if not os.path.exists('points_cropped_half'):
        os.makedirs('points_cropped_half')

    crop(file_name, half=False)
    crop(file_name, half=True)
