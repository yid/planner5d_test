import cv2
import os
import numpy as np
import itertools
from skimage import measure


# search range
param1_values = [1, 3, 5, 7, 9, 11, 13, 15]
param2_values = [2**i for i in range(0, 23, 1)]


# integrated filter function with the best parameters found
def filter(img_binary):
    kernel = np.ones((3, 3), np.uint8)
    img_binary = cv2.morphologyEx(img_binary, cv2.MORPH_CLOSE, kernel)

    labels = measure.label(img_binary, neighbors=8, background=0)
    mask = np.zeros(img_binary.shape, dtype="uint8")

    # loop over the unique components
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue

        # otherwise, construct the label mask and count the
        # number of pixels
        labelMask = np.zeros(img_binary.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)

        # if the number of pixels in the component is sufficiently
        # large, then add it to our mask of "large blobs"
        if numPixels > 2:
            mask = cv2.add(mask, labelMask)
    return mask


def thresh_img(img_gray):
    img_gray = cv2.medianBlur(img_gray, 7)
    img_thresh = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 69, 70)
    return img_thresh


def get_iou(output_img, target_img):
    bool_output_img = output_img > 128
    bool_target_img = target_img > 128

    pos_output = np.count_nonzero(bool_output_img)
    true_pos = np.count_nonzero(np.logical_and(bool_output_img, bool_target_img))
    false_pos = pos_output - true_pos

    neg_output = np.count_nonzero(np.logical_not(bool_output_img))
    true_neg = np.count_nonzero(np.logical_not(np.logical_or(bool_target_img, bool_output_img)))
    false_neg = neg_output - true_neg

    iou = (true_pos + 0.00001) / (true_pos + false_pos + false_neg + 0.00001)

    return iou


total_acc = 0
for image_name in os.listdir("inputs"):
    img_bgr = cv2.imread(os.path.join("inputs", image_name))
    img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
    groundtruth = cv2.imread(os.path.join("walls", image_name), cv2.IMREAD_GRAYSCALE)

    output = thresh_img(img_gray)  # black background, white walls
    output = filter(output)

    output = 255 - output
    total_acc += get_iou(output, groundtruth) / len(os.listdir("inputs"))
    cv2.imwrite(os.path.join("outputs", image_name), output)

print("iou acc {}".format(total_acc))
