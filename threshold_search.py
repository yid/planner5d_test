import cv2
import os
import numpy as np
import itertools


# search range
param1_values = [i for i in range(49, 81, 2)]  # kernel_size
param2_values = [i for i in range(49, 81, 1)]  # C
param3_values = [i for i in range(1, 19, 2)]  # blur size
param4_values = ["median", "gaussian"]  # blur type


def thresh_img(img_gray, param1, param2, param3, param4):
    if param4 == "median":
        if param3 > 1:
            img_gray = cv2.medianBlur(img_gray, param3)
    else:
        if param3 > 1:
            img_gray = cv2.blur(img_gray, (param3, param3))
    img_thresh = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, param1, param2)
    # img_thresh = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, param1, param2)
    # ret, img_thresh = cv2.threshold(img_gray, param1, 255, cv2.THRESH_BINARY)
    return img_thresh


def get_iou(output_img, target_img):
    bool_output_img = output_img > 128
    bool_target_img = target_img > 128

    pos_output = np.count_nonzero(bool_output_img)
    true_pos = np.count_nonzero(np.logical_and(bool_output_img, bool_target_img))
    false_pos = pos_output - true_pos

    neg_output = np.count_nonzero(np.logical_not(bool_output_img))
    true_neg = np.count_nonzero(np.logical_not(np.logical_or(bool_target_img, bool_output_img)))
    false_neg = neg_output - true_neg

    iou = (true_pos + 0.00001) / (true_pos + false_pos + false_neg + 0.00001)

    return iou


max_acc = 0
max_acc_params = None
for param1, param2, param3, param4 in itertools.product(param1_values, param2_values, param3_values, param4_values):
    total_acc = 0
    for image_name in os.listdir("inputs"):
        img_bgr = cv2.imread(os.path.join("inputs", image_name))
        img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
        target = cv2.imread(os.path.join("walls", image_name), cv2.IMREAD_GRAYSCALE)

        output = 255 - thresh_img(img_gray, param1, param2, param3, param4)  # black background, white walls
        total_acc += get_iou(output, target) / len(os.listdir("inputs"))

    if total_acc > max_acc:
        max_acc = total_acc
        max_acc_params = (param1, param2, param3, param4)
    print("trying {} {} {} {}, acc {}".format(param1, param2, param3, param4, total_acc))

print("max acc params {} with acc {}".format(max_acc_params, max_acc))
