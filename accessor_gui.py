import cv2
import os


def access_img(img_path):
    img = cv2.imread(img_path)
    labels_file = open(img_path[:-3] + 'txt', 'w')

    def catch_click(event, x, y, flags, param):
        global mouseX, mouseY
        if event == 1:
            mouseX, mouseY = x, y
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1)
            labels_file.write("{} {}\n".format(y, x))

    cv2.namedWindow('gui')
    cv2.setMouseCallback('gui', catch_click)

    while True:
        cv2.imshow("gui", img)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            break

    labels_file.close()


for file_name in os.listdir("inputs"):
    access_img(os.path.join("inputs", file_name))
